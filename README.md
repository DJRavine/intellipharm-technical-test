# intellipharm-technical-test

# Guide

I used this guide as a basis for the infrastructure setup.  
LINK: <https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose>

## Build and Run

``` bash
docker-compose up -d --build
```

## Config

``` bash
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan config:cache
docker-compose exec app php artisan migrate
```

## Stop

``` bash
docker-compose stop
```

## Down

``` bash
docker-compose down
```

## Clean up everything

``` bash
docker-compose down -v --rmi all --remove-orphans
```