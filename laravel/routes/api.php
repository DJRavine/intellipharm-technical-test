<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// List all member
Route::get('members', 'MemberController@index');

// List all member signups by year
Route::get('members/byyear', 'MemberController@byyear');

// List a single member
Route::get('member/{id}', 'MemberController@show');