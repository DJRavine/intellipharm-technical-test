<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Http\Resources\MemberResource;

class MemberController extends Controller
{
    public function index(Request $request)
    {

      $search = false;
      
      // Get the members
      $members = Member::query();

      if ($request->has('firstname') && !empty($request->input('firstname'))) {
        $members->where('firstname', $request->input('firstname'));
        $search = true;
      }

      if ($request->has('surname') && !empty($request->input('surname'))) {
        $members->where('surname', $request->input('surname'));
        $search = true;
      }

      if ($request->has('email') && !empty($request->input('email'))) {
        $members->where('email', $request->input('email'));
        $search = true;
      }

      if ($search == false) {
        return Member::Paginate(10);
      } else {
        // Return collection of members as a resource
        return MemberResource::collection($members->get());
      }
    }
    
    public function store(Request $request)
    {
      //
    }
    
    public function byyear(Request $request)
    {
      return $membersPerMonth = Member::selectRaw("COUNT(*) members, DATE_FORMAT(joined_date, '%Y') date")
        ->groupBy('date')
        ->get();
    }
    
    public function show($id)
    {
      // Find the member
      $member = Member::findOrFail($id);

      // Return a single member as a resource
      return new MemberResource($member);
    }
    
    public function destroy($id)
    {
      //
    }
}
