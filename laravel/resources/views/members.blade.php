<!-- Stored in resources/views/members.blade.php -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">

        <script type="text/javascript" charset="utf8" crossorigin="anonymous" src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="></script>
        <script type="text/javascript" charset="utf8" crossorigin="anonymous" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" charset="utf8" crossorigin="anonymous" src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
        <script type="text/javascript" charset="utf8" crossorigin="anonymous" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <title>Members</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
        </style>

    </head>
    <body>
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-12 text-center pt-3 pb-2">
                    <h1>Members</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center pt-3 pb-2">
                    <canvas id="members-chart" width="400" height="100"></canvas>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 pb-3">
                    <form class="form-inline justify-content-center">
                        <div class="form-group">
                            <input type="text" class="form-control-sm" id="firstname" placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control-sm" id="surname" placeholder="Surname">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control-sm" id="email" placeholder="Email">
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">

                    <table id="member-table" class="table table-striped table-bordered" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th>ID</th>
                                <th>Firstname</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Joined</th>
                            </tr>
                        </thead>
                        <tbody id="member-data">
                        </tbody>
                        <tfoot class="thead-dark">
                            <tr>
                                <th>ID</th>
                                <th>Firstname</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Joined</th>
                            </tr>
                        </tfoot>
                    </table>

                    <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-end">
                            <li class="page-item"><a class="page-link" id="first" href="#">First</a></li>
                            <li class="page-item"><a class="page-link" id="previous" href="#">Previous</a></li>
                            <li class="page-item disabled"><a class="page-link" href="#"><span id="page-number">1</span></a></li>
                            <li class="page-item"><a class="page-link" id="next" href="#">Next</a></li>
                            <li class="page-item"><a class="page-link" id="last" href="#">Last</a></li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>

        <script type="text/javascript">

            var current_page = 1;
            var first_page = 1;
            var last_page = 1;

            function loadingData() {
                $("#member-data").empty();
                $("#member-data").append( '<tr>\
                    <td colspan="10" class="text-center"><i>LOADING...</i></td>\
                </tr>');
            }

            function noData() {
                $("#member-data").empty();
                $("#member-data").append( '<tr>\
                    <td colspan="10" class="text-center"><i>No Results Found...</i></td>\
                </tr>');
            }

            function getData(page) {
                loadingData();
                $.ajax({
                    url: "/api/members?page=" + page,
                    success: function(result) {
                        console.log(result);
                        processData(result);
                    }
                });
            }

            function getGraphData() {
                loadingData();
                $.ajax({
                    url: "/api/members/byyear",
                    success: function(result) {
                        console.log(result);
                        processGraphData(result);
                    }
                });
            }

            function searchData(page) {
                loadingData();
                $.ajax({
                    url: "/api/members?page=" + first_page + "&firstname=" + $('#firstname').val() + "&surname=" + $('#surname').val() + "&email=" + $('#email').val(),
                    success: function(result) {
                        console.log(result);
                        processData(result);
                    }
                });
            }

            function processData(result) {
                $("#member-data").empty();
                current_page = result.current_page;
                last_page = result.last_page;
                $("#page-number").html(current_page);
                if(result.data.length == 0) {
                    noData();
                } else {
                    $.each(result.data , function(i, member) {
                        $("#member-data").append( '<tr>\
                            <th>' + member.id + '</th>\
                            <th>' + member.firstname + '</th>\
                            <th>' + member.surname + '</th>\
                            <th>' + member.email + '</th>\
                            <th>' + member.gender + '</th>\
                            <th>' + member.joined_date + '</th>\
                        </tr>' );
                    });
                }
            }

            function processGraphData(result) {
                var ctx = document.getElementById('members-chart').getContext('2d');
                var labels = [];
                var data = [];
                $.each(result, function(i, item) {
                    labels.push(item.date);
                    data.push(item.members);
                });
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: 'Sign-ups',
                            data: data,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            }

            $(document).ready(function() {

                // Send initial ajax request for data
                getData(first_page);
                getGraphData();

                // Pagination actions
                $("#first").click( function() {
                    getData(first_page);
                });
                $("#next").click( function() {
                    var new_page = current_page + 1;
                    if(new_page > last_page){
                        new_page = last_page
                    }
                    getData(new_page);
                });
                $("#previous").click( function() {
                    var new_page = current_page - 1;
                    if(new_page < first_page){
                        new_page = last_page
                    }
                    getData(new_page);
                });
                $("#last").click( function() {
                    getData(last_page);
                });

                // Search inputs
                $('#firstname, #surname, #email').on('keyup', _.debounce(function (e) {
                    searchData();
                }, 750));
                

            } );
        </script>
    </body>
</html>
